package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	osUser "os/user"
)

type cUser struct {
	CU osUser.User
}

var t *template.Template
var userList []cUser

func init() {
	t = template.Must(template.ParseGlob("templates/*.gohtml"))
}

func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/user", user)
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func index(w http.ResponseWriter, r *http.Request) {
	err := t.ExecuteTemplate(w, "index.gohtml", nil)
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
	}
}

func user(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var u cUser
		if r.Body != nil {
			err := json.NewDecoder(r.Body).Decode(&u)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			fmt.Println(u)
		}
	}
}
