package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os/user"
	"time"
)

type cUser struct {
	CU user.User
}

func main() {
	var u cUser
	var err error
	var cu *user.User
	b := new(bytes.Buffer)
	je := json.NewEncoder(b)
	for {
		cu, err = user.Current()
		if err != nil {
			log.Fatal(err)
		}
		u.CU = *cu
		je.Encode(u)
		_, err = http.Post("http://localhost:8000/user", "application/json; charset=utf-8", b)
		if err != nil {
			fmt.Println(err.Error())
		}
		time.Sleep(3 * time.Second)
	}
}
